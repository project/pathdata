<?php

/**
 * Implementation of hook_views_handlers().
 */
function pathdata_views_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'pathdata_views'),
    ),
    'handlers' => array(
      'pathdata_views_handler_field_link' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data_alter().
 */
function pathdata_views_views_data_alter(&$data) {
  $data['views']['pathdata_link'] = array(
    'title' => t('Pathdata link'),
    'help' => t('Create a path alias with attached GET data on the fly and render a link of it. Optionally append current exposed filter data.'),
    'field' => array(
      'handler' => 'pathdata_views_handler_field_link',
    ),
  );
}
