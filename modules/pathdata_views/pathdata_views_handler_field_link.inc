<?php

class pathdata_views_handler_field_link extends views_handler_field {

  function query() {
    // do nothing -- to override the parent query.
  }

  function option_definition() {
    $options = parent::option_definition();

    // Override the alter text option to always alter the text.
    $options['alter']['contains']['alter_text'] = array('default' => TRUE);
    $options['alter']['contains']['make_link'] = array('default' => TRUE);
    $options['alter']['pathdata_alias'] = array('default' => '');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // Unset a lot of useless options. We are keeping this handler as clean as
    // possible, since the internal logic is complex enough.
    unset($form['alter']['alt']);
    unset($form['alter']['alter_text']);
    unset($form['alter']['link_class']);
    unset($form['alter']['prefix']);
    unset($form['alter']['query']);
    unset($form['alter']['target']);
    unset($form['alter']['suffix']);
    unset($form['alter']['text']['#dependency']);
    unset($form['alter']['text']['#process']);

    // Alter existing options.
    $form['alter']['text']['#weight'] = -3;
    $form['alter']['make_link']['#weight'] = -2;
    $form['alter']['path']['#weight'] = -1;
    $form['alter']['path']['#title'] = t('Existing system path');
    $form['alter']['path']['#description'] = t('Specify the existing path you wish to alias. You may enter data from this view as per the "Replacement patterns" below. GET parameters defined here will be saved with the alias.');

    // Add our new option for the path source.
    $form['alter']['pathdata_alias'] = array(
      '#type' => 'textfield',
      '#title' => t('Path alias'),
      '#description' => t('Specify an alternative path by which this data can be accessed. You may enter data from this view as per the "Replacement patterns" below. GET parameters defined here won\'t be saved, but passed with the link.'),
      '#process' => array('views_process_dependency'),
      '#dependency' => array('edit-options-alter-make-link' => array(1)),
      '#weight' => 0,
      '#default_value' => $this->options['alter']['pathdata_alias'],
    );
  }

  function add_self_tokens(&$all_tokens, $item) {
    global $_PATHDATA;
    static $tokens = array();

    // All tokens in this method are global, and will not change
    // from result row to result row. So, only do this once.
    if (!empty($tokens)) {
      $all_tokens = array_merge($all_tokens, $tokens);
      return;
    }

    module_load_include('inc', 'pathauto');

    $paths = explode('/', $_GET['q']);
    foreach ($paths as $i => $path) {
      $tokens['[pathdata-arg-' . $i . ']'] = arg($i);
    }

    foreach ($_GET as $key => $value) {
      $tokens['[pathdata-get-' . $key . ']'] = $value;
      $tokens['[pathdata-get-' . $key . '-cleaned]'] = pathauto_cleanstring($value);
    }

    foreach ($_REQUEST as $key => $value) {
      $tokens['[pathdata-request-' . $key . ']'] = $value;
      $tokens['[pathdata-request-' . $key . '-cleaned]'] = pathauto_cleanstring($value);
    }

    $requested_query_string = array();
    foreach ($_PATHDATA['requested'] as $key => $value) {
      if (is_array($value)) {
        foreach ($value as $inner_key => $inner_value) {
          $requested_query_string[] = $key. '[' .$inner_key . ']=' . $inner_value;
        }
      } else {
        $requested_query_string[] = $key . '=' . $value;
      }
    }
    $tokens['[pathdata-requested-query-string]'] = implode('&', $requested_query_string);

    $merged_query_string = array();
    foreach ($_PATHDATA['merged'] as $key => $value) {
      $merged_query_string[] = $key . '=' . $value;
    }
    $tokens['[pathdata-merged-query-string]'] = implode('&', $merged_query_string);
  }

  function document_self_tokens(&$tokens) {
    $tokens['[pathdata-arg-N]'] = t('Path argument <em>N</em>');
    $tokens['[pathdata-get-X]'] = t('GET parameter <em>X</em>');
    $tokens['[pathdata-get-X-cleaned]'] = t('GET parameter <em>X</em> cleaned with <code>pathauto_cleanstring()</code>');
    $tokens['[pathdata-request-X]'] = t('REQUEST parameter <em>X</em>');
    $tokens['[pathdata-request-X-cleaned]'] = t('REQUEST parameter <em>X</em> cleaned with <code>pathauto_cleanstring()</code>');
    $tokens['[pathdata-requested-query-string]'] = t('A string containing all requested GET parameters, in the format of <code>key1=value2&key2=value</code>. The <code>q</code> and <code>page</code> parameters are excluded.');
    $tokens['[pathdata-merged-query-string]'] = t('A string containing all requested GET parameters plus parameters added by the pathdata module, in the format of <code>key1=value2&key2=value</code>. The <code>q</code> and <code>page</code> parameters are excluded.');
  }

  /**
   * Render this field as a link, with the info from a fieldset set by
   * the user.
   */
  function render_as_link($alter, $text, $tokens) {
    $path = $alter['path'];

    // Initiate out different link options.
    $path_options = array();
    $alias_options = array();
    $path_options['html'] = TRUE;
    $alias_options['html'] = TRUE;

    list($path, $path_options['query']) = $this->parse_url($path, $tokens);

    // Don't render a link if we don't have a path, don't want to make it a link
    // or if we are in preview mode. 
    if (empty($path) || !$alter['make_link'] || (arg(0) == 'admin' && arg(1) == 'build')) {
      return $text;
    }

    if (isset($this->options['alter']['language'])) {
      $path_options['language'] = $this->options['alter']['language'];
      $alias_options['language'] = $this->options['alter']['language'];
    }

    // Save the pathdata alias, but not in preview state.
    if (!empty($alter['pathdata_alias']) && $alter['make_link']) {
      list($alias, $alias_options['query']) = $this->parse_url($alter['pathdata_alias'], $tokens);
      pathdata_set_alias($path, $alias, $path_options['query']);
      return l($text, $alias, $alias_options);
    }

    return l($text, $path, $path_options);
  }

  function parse_url($path, $tokens) {
    // Patterns for removing empty pathdata tokens. Usually tokens is reliable
    // in the Views API, but pathdata tokens are wildcard tokens and thus must
    // be removed if they are not defined. Otherwise it will mess upp the path.
    $path_pattern = '@/\[pathdata\-[a-z1-9_-]+\]@';
    $query_pattern = '@[a-z1-9_-]+=\[pathdata\-[a-z1-9_-]+\]&?@';

    // Parse the URL and move the query parameter out of the path.
    $url = parse_url($path);
    if (isset($url['query'])) {
      $path = strtr($path, array('?' . $url['query'] => ''));
      $query = strtr($url['query'], $tokens);
      // Remove empty pathdata tokens.
      // TODO: Replace "All" with something more generic.
      $query = preg_replace($query_pattern, '', $query);
    }

    // html_entity_decode removes <front>, so check whether its different to front.
    if ($path != '<front>') {
      // Use strip tags as there should never be HTML in the path.
      // However, we need to preserve special characters like " that
      // were removed by check_plain().
      $path = strip_tags(html_entity_decode(strtr($path, $tokens)));
      // Remove empty pathdata tokens.
      $path = preg_replace($path_pattern, '', $path);
    }

    return array($path, $query);
  }
}
