
INTRODUCTION
------------

This package contains two modules:

* pathdata
* pathdata_views

The first module, pathdata, is just an API module. It does nothing on it's
own. The pathdata_view module implement this API and exposes it in an
interesting way to the Views UI.

The pathdata module makes it possible to save path aliases with GET data in
them. An example of this is that you can make an alias of this URL:

    gallery/search?term1=123&term2=456

That instead could look like this:

    gallery/search/sofa/red


UNDER THE HOOD
--------------

PATHDATA

What this module actually does, it that it wraps the path_set_alias() function
with some additional functionality. It saves the given GET data in a separate
table. Then, when a page with an alias containing GET data is initiated, the
GET data gets injected to the page request. That is, the data will exist in GET
but will not be visible in the user's browser.

The initiation of the data happens in hook_init() of the pathdata module. This
is far from optimal, since the information won't be available until that hook
fires. But it's the best way of doing it in Drupal.

PATHDATA_VIEWS

This module provides a field handler that, while rendering a link, can create
an alias on-the-fly with information from the current request. That could be
data from the GET or REQUEST variables. That data is available as tokens to the
Views UI.
