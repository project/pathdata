<?php

/**
 * API function for saving path alias with additional GET data.
 *
 * @param $path
 *   The Drupal path that you want to create an alias for. Should not contain
 *   any GET parameters.
 *
 * @param $alias
 *   The alias you want to associate $path with. Should not contain any GET
 *   parameters.
 *
 * @param $data
 *   The GET data that you want to add to $path and $alias.
 *
 * @param $language
 *   Language id for this alias, if it's language specific.
 */
function pathdata_set_alias($path, &$alias, $data = NULL, $language = '', $reset = FALSE) {
  static $cache = array();
  $alias = trim($alias, '/');

  // This is probably a query string formatted for l(). We need to convert
  // it to an array before going further.
  if (!is_array($data)) {
    $data_string = $data;
    $data = array();
    $parameters = explode('&', (string)$data_string);
    foreach ($parameters as $parameter) {
      list($key, $value) = explode('=', $parameter);
      if (!empty($key) && !empty($value)) {
        $data[$key] = $value;
      }
    }
  }

  // Check if we already set this alias, with this data, on this request.
  if (!isset($cache[$alias]) || $cache[$alias] != $data) {
    // Never set aliases that are the same as the path.
    if ($path != $alias) {
      $pid = pathdata_get_pid($alias);
      // Let other modules alter this data.
      drupal_alter('pathdata', $alias, $data);
      // Now when modules has been able to alter the data, check if we still
      // have required parameters to create an alias.
      if (!empty($path) && !empty($alias)) {
        path_set_alias($path, $alias, $pid, $language);
        // We need the pid for the newly created path alias.
        if (empty($pid)) {
          $pid = db_last_insert_id('url_alias', 'pid');
        }
        if ($pid) {
          pathdata_set_data($pid, $data);
          $cache[$alias] = $data;
        }
      }
    }
  }
}

/**
 * Convenient function for redirecting.
 *
 * Often after creating a pathdata alias you want to redirect
 * to that alias. Due to the nature of a pathdata alias, GET
 * parameters might need to be passed on (or excluded) to the
 * redirect. This function makes that easier.
 *
 * By default this function works exactly as drupal_goto().
 */
function pathdata_goto($path, $add_params = FALSE, $excluded_params = array()) {
  $query = array();

  if ($add_params) {
    $query = $_GET;
  }

  // Make the query array, into a valid string.
  $query = drupal_query_string_encode($query, $excluded_params);
  drupal_goto($path, $query);
}

/**
 * Implementation of hook_init().
 */
function pathdata_init() {
  // Initiate the pathdata data for this request.
  pathdata_init_data();
}

/**
 * Initiates the data in $_GET for a given path and set $_PATHDATA with
 * useful data for others to access.
 */
function pathdata_init_data($path = NULL) {
  // Global variable where one can access information about pathdata GET
  // parameters in different states.
  global $_PATHDATA;

  $_PATHDATA = array(
    // This key contains all GET parameters that was requested, except
    // the 'q' and 'pager' parameter. Those need to be dealt with
    // separately.
    'requested' => array(),
    // This key contains all GET parameters that was added by the pathdata
    // system (saved with an alias).
    'added' => array(),
    // This key contains the merged version of all requested and added GET
    // parameters.
    'merged' => array(),
    // This key contains the current path id.
    'pid' => 0
  );

  $_PATHDATA['requested'] = $_GET;
  unset($_PATHDATA['requested']['q']);
  unset($_PATHDATA['requested']['page']);

  if (!isset($path)) {
    $path = drupal_get_path_alias($_GET['q']);
  }

  // Only initiate the data if we actually requested this path alias.
  // We must use REQUEST here because GET is aleady normalized from the
  // bootstrap.
  if ($_REQUEST['q'] == $path) {
    $_PATHDATA['pid'] = pathdata_get_pid($path);
    if ($_PATHDATA['pid']) {
      $_PATHDATA['added'] = pathdata_get_data($_PATHDATA['pid']);
      if (!empty($_PATHDATA['added'])) {
        $_GET = array_merge($_GET, $_PATHDATA['added']);
        // This global variable is supposed to be able to be passed into l() as the
        // query addition. So we can't have the 'q' parameter here, not do we add
        // the 'page', since you often want to deal with that by your self.
        $_PATHDATA['merged'] = $_GET;
        unset($_PATHDATA['merged']['q']);
        unset($_PATHDATA['merged']['page']);
      }
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Returns the path alias id for a given path.
 */
function pathdata_get_pid($path) {
  $cache = ctools_static(__FUNCTION__, array());
  if (!isset($cache[$path])) {
    $cache[$path] = db_result(db_query('SELECT pid FROM {url_alias} WHERE dst = "%s"', $path));
  }
  return $cache[$path];
}

/**
 * Fetches the data that should the merged into the $_GET variable.
 */
function pathdata_get_data($pid) {
  $cache = ctools_static(__FUNCTION__, array());
  if (!isset($cache[$pid])) {
    $data = db_result(db_query('SELECT data FROM {pathdata} WHERE pid = %d', $pid));
    // Make sure we return correct data type.
    if (empty($data)) {
      $cache[$pid] = array();
    }
    else {
      $cache[$pid] = unserialize($data);
    }
  }
  return $cache[$pid];
}

/**
 * Saves the data related to a path alias id.
 *
 * @todo Be smarter and update or delete records here also.
 */
function pathdata_set_data($pid, $data = array()) {
  if (!empty($data)) {
    // Never allow to save the 'q' parameter to an alias, since that can
    // cause major confussion.
    if (isset($data['q'])) {
      unset($data['q']);
    }

    $record = new stdClass;
    $record->pid = $pid;
    $record->data = $data;

    // We've prevented duplicate data records per path on a database level. That's
    // the most solid way of doing it. So we need to ignore duplicate entry
    // warnings, thus prefixing this function with @.
    @drupal_write_record('pathdata', $record);
  }
}

/**
 * Implementation of hook_form_alter().
 */
function pathdata_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'path_admin_delete_confirm') {
    $form['#submit'][] = 'pathdata_path_admin_delete_submit';
  }
}

/**
 * Submit handler that deletes pathdata record when aliases are manually
 * removed.
 */
function pathdata_path_admin_delete_submit(&$form, &$form_state) {
  db_query('DELETE FROM {pathdata} WHERE pid = %d', $form_state['values']['pid']);
}
