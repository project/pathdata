<?php

/**
 * Implementation of hook_schema().
 */
function pathdata_schema() {
  return array(
    'pathdata' => array(
      'description' => 'Table that holds get parameters to implement for a specific path alias',
      'fields' => array(
        'pdid' => array(
	  'description' => 'The primary identifier for a pathdata record.',
	  'type' => 'serial',
	  'unsigned' => TRUE,
	  'not null' => TRUE,
        ),
        'pid' => array(
	  'description' => 'The primary identifier for a path alias.',
	  'type' => 'int',
	  'unsigned' => TRUE,
	  'not null' => TRUE,
        ),
        'data' => array(
	  'type' => 'varchar',
	  'not null' => FALSE, 
	  'length' => 255,
	  'serialize' => TRUE,
	),
      ),
      'primary key' => array('pdid', 'pid'),
      // Prevent duplicate data for all paths.
      'unique keys' => array('unique_data' => array('pid', 'data')),
    ),
  );
}

/**
 * Implementation of hook_install().
 */
function pathdata_install() {
  drupal_install_schema('pathdata');
  // By the nature of this module, we need to have our implementation of
  // hook_init() to fire very early.
  db_query('UPDATE {system} SET weight = -9999 WHERE name = "pathdata"');
}

/**
 * Implementation of hook_uninstall().
 */
function pathdata_uninstall() {
  drupal_uninstall_schema('pathdata');
}
